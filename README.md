## Description
 Play Market Permission Parser
## Requirements
 Ubuntu 16.04 LTS
 Perl >= 5.16
 Modern::Perl;
 
 Mojolicious::Lite;
 
 Mojo::UserAgent;
 
 Mojo::DOM;
 
 Data::Dumper;
 
 IO::Socket::SSL;
 
 DBI;
 
 DBD::mysql;
 
 SQL::Abstract::More;
 
 File::Fetch;
 
 phantomjs http://phantomjs.org/

 UWSGI
 ##  Systemd service
 root@ubuntu-app:/var/www/playmarket-parser# cat /etc/systemd/system/playmarket-parser.service 
 [Unit]
 Description=Android Parser Mojo Lite 'uwsgi'
 After=syslog.target
 After=network.target
 
 [Service]
 ExecStart=/usr/bin/uwsgi --ini /etc/uwsgi/uwsgi.ini --env QT_QPA_PLATFORM=offscreen
 Restart=always
 KillSignal=SIGQUIT
 Type=notify
 StandardError=syslog
 NotifyAccess=all
 
 [Install]
 WantedBy=multi-user.target
 root@ubuntu-app:/var/www/playmarket-parser# 
## UWSGI ini  /etc/uwsgi/uwsgi.ini 
[uwsgi]
   http-socket = 127.0.0.1:5000
   http-socket-modifier1 = 5
   psgi = /var/www/playmarket-parser/main.pl
   logger = file:/var/log/uwsgi/app.log
   log-maxsize=15000000
   uid = root
   gid = root
   fs-reload = /var/www/playmarket-parser/
   lazy-apps = true
   processes = 2
## Apache mod proxy 

    <VirtualHost *:666>
    ServerName playparser8633.gotdns.ch
    ProxyPass / http://localhost:5000/
    ProxyPassReverse / http://localhost:5000/
    </VirtualHost>   
   
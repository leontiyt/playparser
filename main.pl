#!/usr/bin/env perl
use strict;
use warnings;
use Modern::Perl;
use Mojolicious::Lite;
use Mojo::UserAgent;
use Mojo::DOM;
use Data::Dumper;
use IO::Socket::SSL;
use DBI;
use DBD::mysql;
use SQL::Abstract::More;
use File::Fetch;


#mysql connection
my %config = do '/var/www/playmarket-parser/sql_config.pl';
our $dbhost = $config{'host'};
our $dbport = $config{'port'};
our $dbname = $config{'database'};
our $dbuser = $config{'user'};
our $dbpass = $config{'password'};
our $dbh = DBI->connect("dbi:mysql:database=$dbname;host=$dbhost;port=$dbport",
    $dbuser,
    $dbpass,
    { AutoCommit => 1, RaiseError => 1 }
) or die $DBI::errstr;
#

my $ua = Mojo::UserAgent->new;
my constant  $play_link = 'https://play.google.com/store/apps/details?';
our $sqlBuilder = SQL::Abstract::More->new(sql_dialect => "Oracle");


helper username => sub {
        my $c =shift;
        return $c->session->{username}
    };

plugin 'PODRenderer'; #Documentation browser under "/perldoc"
get '/' => 'index';
get '/login' => sub{
        my $c = shift;
        $c->session->{username} = "";
        $c->render(template => 'login');
    };
post '/requset/package' => sub {
        my $c = shift;
        my $language = $c->param('language');
        my $package = $c->param('package');
        my $json; # render some info
        my $last_id; #last id , inserted into package swl table
        my $name; #name like Telegramm
        my $full_name; #like MediBang Inc.
        #select package_id FROM package where package_name = 'com.whatsapp'
        my $found_id;
        eval{
            my ($statement, @bind) = $sqlBuilder->select(-from => 'package',
                -columns => [ 'package_id' ],
                -where => [ package_name => $package ]);
            my $sql = $dbh->prepare($statement);
            $sqlBuilder->bind_params($sql, @bind);
            $sql->execute() or  $sql->errstr;
            $found_id = $sql->fetchrow_array;
        };
        say STDERR "found id".$found_id;
        if (!$found_id) {
            #if  package not found in the DB
            my $link = $play_link . "id=$package&hl=$language";
            my $res = $ua->get($link)->result;

            if ($res->is_success) {say STDERR ">>>>Connected..."}
            elsif ($res->is_error) {say STDERR $res->message}
            elsif ($res->code == 301) {say STDERR $res->headers->location}
            else {say STDERR 'Whatever...'}

            my $cmd = "/usr/bin/phantomjs /var/www/playmarket-parser/public/js/phantom.js $link";
            my $output = `$cmd`;
            my $dom = Mojo::DOM->new($output);

            my $result = ($dom->find('div.g3VIld')->each)[0];
               $name = ($result->find('div.miUA7 span')->each)[0]->text;
               $full_name = ($result->find('div.LruL2b span')->each)[0]->text;

            my ($statement, @bind) = $sqlBuilder->insert(
                -into   => 'package',
                -values => {
                    name         => $name,
                    full_name    => $full_name,
                    package_name => $package,
                    language     => $language
                }
            );
            my $sql = $dbh->prepare($statement);
            $sqlBuilder->bind_params($sql, @bind);
            if (!$sql->execute()) {
                say STDERR "ERR:cannot insert into package , " . $sql->errstr;
                return 1;
            }
            else {
                $sql = $dbh->prepare($sqlBuilder->select(-from => 'package', -columns => [ 'MAX(package_id)' ]));
                $sql->execute();
                $last_id = $sql->fetchrow_array;
            }

            my @result = $result->find('div.yk0PFc')->each;

            my $perm_group;
            my $perm_name;
            my $perm_group_img;
            my $image_path;
            for my $res_arr (@result) {
                for ($res_arr->find('div.tDgPAd span.BR7Zgd span')->each) {
                    $perm_group = $_->text;
                    say STDERR $perm_group;
                    $image_path = $perm_group;
                    if ($image_path =~/\//) {
                        $image_path =~ s/\//_/g;
                    }
                }
                for ($res_arr->find('div.tDgPAd img.T75of.mu6etb')->map(attr => 'src')->each) {
                    $perm_group_img = $_;
                    if(2 < 1 ){
                        eval{
                            my $ff = File::Fetch->new(uri => $perm_group_img);
                            my $file = $ff->fetch( to => '/var/www/playmarket-parser/public/img/'.$perm_group.'.png' );
                            if ($file) {

                            }
                        };
                        if ($@) {
                            say STDERR "ERR writing file. $@";
                        }
                    }
                }
                for ($res_arr->find('ul.l5zUie li.NLTG4 span')->each) {
                    $perm_name = $_->text;
                    say STDERR $perm_name;
                    my ($statement1, @bind1) = $sqlBuilder->insert(
                        -into   => 'permission',
                        -values => {
                            perm_group => $perm_group,
                            perm_name  => $perm_name,
                            image_path => '/img/'.$image_path.'.png',
                            package_id => $last_id
                        }
                    );
                    $sql = $dbh->prepare($statement1);
                    $sqlBuilder->bind_params($sql, @bind1);
                    $sql->execute()
                }
            }

        }
        else {
            $last_id = $found_id;
            my ($statement1, @bind1) = $sqlBuilder->select(
                -from    => 'package',
                -columns => [
                    'name',
                    'full_name'
                ],
                -where   => { 'package_id' => $last_id }
            );
            my $sql = $dbh->prepare($statement1);
            $sqlBuilder->bind_params($sql, @bind1);
            $sql->execute() or  $sql->errstr;
            ($name,$full_name) = $sql->fetchrow_array;
        }

        #          my $dom = Mojo::DOM->new(<<'HTML');

        #          #my @result = ($dom->find('div.fnLizd div.tDgPAd span.BR7Zgd')->each);
        #          my @result = $dom->find('div.fnLizd div.yk0PFc')->each;
        #
        #          #$_->text for $dom->find('div.days')->each;
        #
        #          for my $res_arr (@result) {
        #                say STDERR  $_->text for $res_arr->find('div.tDgPAd span.BR7Zgd span')->each;
        #                say STDERR  $_ for $res_arr->find('div.tDgPAd img.T75of.mu6etb')->map(attr => 'src')->each;
        #                say STDERR $_->text for ($res_arr->find('ul.l5zUie li.NLTG4 span')->each);
        #                say STDERR "ITERATION--------";
        #         }
        #          $c->render(json => \@perm_name);
        #render permissions;
        #select id, perm_group, perm_name, package_id FROM permission where package_id = ?

        my ($statement1, @bind1) = $sqlBuilder->select(
            -from    => 'permission',
            -columns => [
                'perm_group',
                'perm_name',
                'image_path'
            ],
            -where   => { 'package_id' => $last_id }
        );
        my $hash = $dbh->selectall_arrayref($statement1, { Slice => {} }, @bind1);
        $c->render(json => $hash);

        $json->{name} = $name;
        $json->{full_name} = $full_name;
        $json->{package} = $package;
        $json->{language} = $language;
        $json->{permissions} = $hash;

        $c->render(json => $json);
    };
post '/request/login' => sub {
        my $c = shift;
        my $username = $c->param('email');
        my $password = $c->param('password');
        #select id FROM users_names where username = 'leontiy.t@gmail.com' and password = 'ehfufy1242!'
        my ($statement, @bind) = $sqlBuilder->select(
            -from => "users_names",
            -columns => [
                "id"
            ],
            -where => {
                "username" => $username,
                "password" => $password
            }
        );
       # $json = ($dbh->selectrow_array($statement, {}, @bind))[0] ? 1 : 0;

        my $sql = $dbh->prepare($statement);
        $sqlBuilder->bind_params($sql, @bind);
        $sql->execute() or  $sql->errstr;
        my $json = $sql->fetchrow_array;
        say STDERR "JSON: $json";
        if ($json) {
            $json = 1;
            $c->session->{username} = "$username";
            $c->session(expiration => 3600); #Session  timelive
        }else{
            $json = 0;
        }

        $json = '{"resp":"'.$json.'"}';
        $c->render(text => "$json");
    };

app->secrets([ 'dfsdfsdDHSsccdHg' ]);
app->start;
